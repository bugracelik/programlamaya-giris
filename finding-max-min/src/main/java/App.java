import com.csystem.promgramlamayagiris.model.Array;
import com.csystem.promgramlamayagiris.printcontroller.PrintController;

public class App {
    public static void main(String[] args) {

        Array array = new Array();

        //printcontrollerdan nesne aldım
        PrintController printController = new PrintController();
        //Arrayi ekrana yazar
        printController.getArray();

        System.out.println("**************");
        int maxValue = printController.getMaxValue(array.getArray());
        System.out.println("max value : " + maxValue);
        System.out.println("***************");
        int minValue = printController.getMinValue(array.getArray());
        System.out.println("min value : " + minValue);
    }
}

