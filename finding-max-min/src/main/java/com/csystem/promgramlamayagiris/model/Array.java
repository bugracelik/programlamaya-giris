package com.csystem.promgramlamayagiris.model;

import java.util.Arrays;

public class Array {
    private int [] array = {4, 8, 3, 1, 18, 9, 21, 20, 5, 17};

    public Array() {
    }

    public int[] getArray() {
        return this.array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    @Override
    public String toString() {
        return "Array{" +
                "array=" + Arrays.toString(array) +
                '}';
    }

}
