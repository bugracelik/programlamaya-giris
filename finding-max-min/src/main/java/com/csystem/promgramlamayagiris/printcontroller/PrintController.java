package com.csystem.promgramlamayagiris.printcontroller;

import com.csystem.promgramlamayagiris.model.Array;
import com.csystem.promgramlamayagiris.printservice.PrintServiceImpl;

public class PrintController {


    public Array getArray() {
        PrintServiceImpl printService = new PrintServiceImpl();
        return printService.getArray();
    }


    public int getMaxValue(int [] array) {
        PrintServiceImpl printService = new PrintServiceImpl();
        return printService.findMaxValue(array);
    }

    public int getMinValue(int[] array) {
        PrintServiceImpl printService = new PrintServiceImpl();
        return printService.findMinValue(array);
    }
}
