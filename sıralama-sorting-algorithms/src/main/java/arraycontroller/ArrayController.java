package arraycontroller;

import arrayservice.ArrayService;
import model.Array;

public class ArrayController {

    public int[] sortMaxMin(Array array) {
        ArrayService arrayService = new ArrayService();
        return arrayService.sortMaxMin(array);
    }

    public int[] sortMinMax(Array array) {
        ArrayService arrayService = new ArrayService();
        return arrayService.sortMinMax(array);
    }
}
