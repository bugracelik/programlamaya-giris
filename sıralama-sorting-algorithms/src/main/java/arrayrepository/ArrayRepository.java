package arrayrepository;

import model.Array;

public class ArrayRepository {
    public int[] sortMaxMin(Array array) {
        int[] array1 = array.getArray();
        int k, i;
        for (k = 0; k < array1.length - 1; k++){
            for (i = 0;  i < array1.length - 1; i++){
                if (array1[i] < array1[i + 1]){
                    int temp = array1[i];
                    array1[i] = array1[i + 1];
                    array1[i + 1] = temp;
                }
            }
        }
        return array1;
    }

    public int[] sortMinMax(Array array) {
        int[] array1 = array.getArray();
        int k, i;
        for (k = 0; k < array1.length - 1; k++){
            for (i = 0;  i < array1.length - 1; i++){
                if (array1[i] > array1[i + 1]){
                    int temp = array1[i];
                    array1[i] = array1[i + 1];
                    array1[i + 1] = temp;
                }
            }
        }
        return array1;
    }
}
