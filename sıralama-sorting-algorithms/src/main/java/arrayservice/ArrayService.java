package arrayservice;

import arrayrepository.ArrayRepository;
import model.Array;

public class ArrayService {
    public int[] sortMaxMin(Array array) {
        ArrayRepository arrayRepository = new ArrayRepository();
       return arrayRepository.sortMaxMin(array);
    }

    public int[] sortMinMax(Array array) {
        ArrayRepository arrayRepository = new ArrayRepository();
        return arrayRepository.sortMinMax(array);
    }
}
