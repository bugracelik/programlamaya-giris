package sort;

import arraycontroller.ArrayController;
import arrayservice.PrintArray;
import model.Array;

public class AppSorting {

    public static void main(String[] args) {
        Array array = new Array();
        ArrayController arrayController = new ArrayController();
        int[] sortmaxmin = arrayController.sortMaxMin(array);
        PrintArray.printArray(sortmaxmin);
        System.out.println("********************");
        int[] sortminmax = arrayController.sortMinMax(array);
        PrintArray.printArray(sortminmax);


    }
}
